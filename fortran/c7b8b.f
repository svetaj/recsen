      PROGRAM C7B8B
C===================================================================
C GLAVNI PROGRAM = GENERISE TABELU 7B8B KODA . REZULTAT SE STAMPA
C                  NA stdio.
C===================================================================
C
      DIMENSION IB(8),IC(5,8,256),IDN(9),IPOI(5)
C
      DO 10 I=1,9
          IDN(I)=0
10    CONTINUE
C
      DO 15 I=1,5
          IPOI(I)=0
15    CONTINUE
C
      DO 90 IB1=0,1
      DO 80 IB2=0,1
      DO 70 IB3=0,1
      DO 60 IB4=0,1
      DO 50 IB5=0,1
      DO 40 IB6=0,1
      DO 30 IB7=0,1
      DO 20 IB8=0,1
C-----------------------------------------------------------------
          IB(1)=IB1
          IB(2)=IB2
          IB(3)=IB3
          IB(4)=IB4
          IB(5)=IB5
          IB(6)=IB6
          IB(7)=IB7
          IB(8)=IB8
C
          II=0
          DO 100 IJ=1,8
              II=II+IB(IJ)
100       CONTINUE
          DO 110 IJ=0,8
              IF(II.EQ.IJ)IDN(IJ+1)=IDN(IJ+1)+1
110       CONTINUE
          DO 120 IJ=1,5
              IF(II.EQ.(IJ+1))THEN
                  IPOI(IJ)=IPOI(IJ)+1
                  DO 130 L=1,8
                       IC(IJ,L,IPOI(IJ))=IB(L)
130               CONTINUE
              ENDIF
120       CONTINUE
C-----------------------------------------------------------------
20    CONTINUE
30    CONTINUE
40    CONTINUE
50    CONTINUE
60    CONTINUE
70    CONTINUE
80    CONTINUE
90    CONTINUE
C
1000  FORMAT(T5,8I1,T20,I1,T30,8I1,T45,I1)
C
      PRINT *,' POINTERI =',IPOI
      DO 140 IJ=1,9
          PRINT *,' DISPARNOST =',(IJ-1-4)*2,' , BROJ RECI =',IDN(IJ)
140   CONTINUE
C
C DISPARNOST=0
C
      ICOUN=0
      DO 150 IJ=1,IPOI(3)
          ICOUN=ICOUN+1
          NM1=1
          NM2=2
          WRITE(6,1000)(IC(3,L,IJ),L=1,8),NM1,(IC(3,L,IJ),L=1,8),NM2
          IF(ICOUN.EQ.128)GOTO 99
150   CONTINUE
C
C DISPARNOST=+/-2
C
      IJ=0
160   CONTINUE
          ICOUN=ICOUN+1
          IJ=IJ+1
          IF(IJ.GT.IPOI(2))GOTO 165
          IF(IJ.GT.IPOI(4))GOTO 165
          NM1=2
          NM2=1
          WRITE(6,1000)(IC(4,L,IJ),L=1,8),NM1,(IC(2,L,IJ),L=1,8),NM2
          IF(ICOUN.EQ.128)GOTO 99
      GOTO 160
C
C DISPARNOST=+/-4
C
165   CONTINUE
      IJ=0
170   CONTINUE
          ICOUN=ICOUN+1
          IJ=IJ+1
          IF(IJ.GT.IPOI(1))GOTO 99
          IF(IJ.GT.IPOI(5))GOTO 99
          NM1=2
          NM2=1
          WRITE(6,1000)(IC(5,L,IJ),L=1,8),NM1,(IC(1,L,IJ),L=1,8),NM2
          IF(ICOUN.GT.128)GOTO 99
      GOTO 170
C
99    STOP
      END
