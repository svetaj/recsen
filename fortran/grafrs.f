      SUBROUTINE GRAFRS(XVEC,YVEC,IDIM,IM,ISTY,IYMIN,IYMAX,XMIN,XMAX)
C=========================================================================
C   PODPROGRAM   = CRTA KRIVU ZAVISNOSTI VEROVATNOCE GRESKE OD SREDNJE
C                  PRIMLJENE OPTICKE SNAGE, U LOG10 RAZMERI PO Y-OSI
C
C   ULAZNE PROMENLJIVE:
C
C    (XVEC,YVEC)  = VEKTORI SA PAROVIMA TACAKA
C    IDIM         = BROJ TACAKA
C    IM           = CRTANJE OKVIRA I OSA  ; 0 - NE , 1 - DA
C    IST          = STIL CRTANJA LINIJE
C    IYMIN        = YMIN = 1.0 E IYMIN
C    IYMAX        = YMAX = 1.0 E IYMAX
C    XMIN         = XMIN     
C    XMAX         = XMAX     
C=========================================================================
C
      DIMENSION XVEC(*),YVEC(*)
      DOUBLE PRECISION XVEC,YVEC
      CHARACTER DUMMY*1,STR*4,STR1*4,AAA*80,IMEM*12,BBB*80,CCC*80,DDD*80
      INTEGER ISTR(1),III(20)
      EQUIVALENCE (STR,ISTR(1)),(III(1),AAA)
C
C INICIJALIZACIJA
C
      ICOL=1
      CALL PPLNST(ISTY)
      CALL PPLNIN(ICOL)
      AL10=ALOG(10.)
      DO 13 IK=1,IDIM
          IF(YVEC(IK).LT.1.E-35)YVEC(IK)=1.E-35
13    CONTINUE
C
      ICOUN=ICOUN+1
      IF(ICOUN.GT.2.AND.IM.GT.0)THEN
         IF(INDEX.EQ.1)THEN
             PRINT *,' PRITISNI <RETURN> ZA NASTAVAK'
             READ *,DUMMY
             CALL LLPAGE
         ENDIF
         IF(INDEX.EQ.2)THEN
             PRINT *,' PROMENI PAPIR !'
             PRINT *,' PRITISNI <RETURN> ZA NASTAVAK'
             READ *,DUMMY
         ENDIF
         ICOUN=1
      ENDIF
C
      PRINT *,' EKRAN/PLOTER/PRINTER/MAKRO/NISTA (1/2/3/4/5)?'
      READ *,INDEX
C
      IF(INDEX.EQ.1)THEN
         XSTART=10.
         YSTART=6.
         CALL GOSEL(10)
         IF(INDEX.EQ.1)CALL PLOTS(0,0,11)
      ELSE IF(INDEX.EQ.2)THEN
C------------------------------
C!       ZA A0 PLOTER         !
C!       XSTART=-0.3          !
C------------------------------
C!       ZA A3 PLOTER         !
         XSTART=3.3           
C------------------------------
         YSTART=6.5
         CALL GOSEL(11)
         CALL PLOTS(0,0,11)
      ELSE IF(INDEX.EQ.3)THEN
         DO 5 I=1,IDIM
            PRINT *,' I= ',I,'   X= ',XVEC(I),'   Y= ',YVEC(I)
5        CONTINUE
         GOTO 99
      ELSE IF(INDEX.EQ.4)THEN
         XSTART=4.
         YSTART=6.7
         PRINT *,' UNESI IME MAKROA'
         READ *,IMEM
         CALL OPENM(IMEM,IOSTAT)
         IF(IOSTAT.NE.0)THEN
            PRINT *,'** GRAFRS: GRESKA PRI OTVARANJU 2D BAZE PODATAKA !'
            GOTO 99
         ENDIF
      ELSE IF(INDEX.EQ.5)THEN
         GOTO 99
      ENDIF
C
      XX=13.
      YY=17.
C------------------------------------------------------------------
C CRTANJE OKVIRA I OBELEZAVANJE OSA
C------------------------------------------------------------------
      IF(IM.GT.0)THEN
          IF(INDEX.EQ.4)THEN
             CALL BOLLIN(XSTART,YSTART,XSTART+XX,YSTART,ICOL,0)
             CALL BOLLIN(XSTART+XX,YSTART,XSTART+XX,YSTART+YY,ICOL,0)
             CALL BOLLIN(XSTART+XX,YSTART+YY,XSTART,YSTART+YY,ICOL,0)
             CALL BOLLIN(XSTART,YSTART+YY,XSTART,YSTART,ICOL,0)
          ELSE
             CALL PLOT(XSTART,YSTART,3)
             CALL PLOT(XSTART+XX,YSTART,2)
             CALL PLOT(XSTART+XX,YSTART+YY,2)
             CALL PLOT(XSTART,YSTART+YY,2)
             CALL PLOT(XSTART,YSTART,2)
          ENDIF
C
          NX=IFIX(ABS(XMAX-XMIN))/2+1
          NY=IYMAX-IYMIN+1
          ICOUN=IYMIN
          X1=XSTART-0.1
          Y1=YSTART
          X2=XSTART-1.
          Y2=YSTART-0.2
          DDY=YY/FLOAT(NY-1)
C
          AAA='Verovatnoca greske'
          IF(INDEX.EQ.4)THEN
             CALL BOLTXT(XSTART-2.,YSTART+5.,ICOL,2,0.4,90.,18,III)
          ELSE
             CALL SYMBOL(XSTART-2.,YSTART+5.,0.4,III,90.,18)
          ENDIF
C
          DO 10 I=1,NY
              IF(I.NE.0)THEN
                  IF(INDEX.EQ.4)THEN
                      CALL BOLLIN(X1,Y1,X1+0.2,Y1,ICOL,ISTY)
                  ELSE
                      CALL PLOT(X1,Y1,3)
                      CALL PLOT(X1+0.2,Y1,2)
                  ENDIF
              ENDIF
              CALL INTCHR(10,STR,2) 
              IF(INDEX.EQ.4)THEN
                  CALL BOLTXT(X2,Y2,ICOL,2,0.4,0.,2,ISTR(1))
              ELSE
                  CALL SYMBOL(X2,Y2,0.4,ISTR(1),0.,2)
              ENDIF
              ICC=ICOUN
              IF(ICC.LT.0)ICC=-ICC
              CALL INTCHR(ICC,STR1,2) 
              IF(ICOUN.LT.0)THEN
                  STR='-'//STR1(1:3)
              ELSE
                  STR=STR1
              ENDIF
              IF(ICC.LT.10)XXX=0.45
              IF(ICC.GE.10)XXX=0.35
              IF(INDEX.EQ.4)THEN
                  CALL BOLTXT(X2+XXX,Y2+0.5,ICOL,2,0.2,0.,3,ISTR(1))
              ELSE
                  CALL SYMBOL(X2+XXX,Y2+0.5,0.2,ISTR(1),0.,3)
              ENDIF
              Y1=Y1+DDY
              Y2=Y2+DDY
              ICOUN=ICOUN+1
10        CONTINUE
C
          IF(INDEX.EQ.4)THEN
             AAA='Srednja primljena opticka snaga [dBm]'
             CALL BOLTXT(XSTART-0.2,YSTART-2.,ICOL,2,0.4,0.,37,III)
             AAA='SLIKA'
C             CALL BOLTXT(XSTART,YSTART-5.0,ICOL,2,0.4,0.,5,III)
             AAA=' Verovatnoca greske u funkciji   '
C             CALL BOLTXT(XSTART+3.5,YSTART-5.0,ICOL,2,0.3,0.,37,III)
             AAA=' srednje primljene opticke snage '
C             CALL BOLTXT(XSTART+3.5,YSTART-5.7,ICOL,2,0.3,0.,37,III)
          ELSE
             AAA='Srednja primljena opticka snaga [dBm]'
             CALL SYMBOL(XSTART-0.2,YSTART-2.,0.4,III,0.,37)
             AAA='SLIKA'
             CALL SYMBOL(XSTART,YSTART-5.0,0.4,III,0.,5)
             AAA=' Verovatnoca greske u funkciji   '
C             CALL SYMBOL(XSTART+3.5,YSTART-5.0,0.3,III,0.,37)
             AAA=' srednje primljene opticke snage '
C             CALL SYMBOL(XSTART+3.5,YSTART-5.7,0.3,III,0.,37)
          ENDIF
C
          COUN=XMIN
          DCOUN=(XMAX-XMIN)/FLOAT(NX-1)
          X1=XSTART
          Y1=YSTART-0.1
          X2=XSTART-0.8
          Y2=YSTART-0.7
          DDX=XX/FLOAT(NX-1)
          DO 20 I=1,NX
              IF(I.NE.0)THEN
                  IF(INDEX.EQ.4)THEN
                      CALL BOLLIN(X1,Y1,X1,Y1+0.2,ICOL,ISTY)
                  ELSE
                      CALL PLOT(X1,Y1,3)
                      CALL PLOT(X1,Y1+0.2,2)
                  ENDIF
              ENDIF
              ICC=IFIX(COUN)
              IF(COUN.LT.0.)ICC=-ICC
              CALL INTCHR(ICC,STR1,2)
              IF(COUN.LT.0.)THEN
                  STR='-'//STR1(1:3)
              ELSE
                  STR=STR1
              ENDIF
              IF(INDEX.EQ.4)THEN
                 CALL BOLTXT(X2,Y2,ICOL,2,0.4,0.,3,ISTR(1))
              ELSE
                 CALL SYMBOL(X2,Y2,0.4,ISTR(1),0.,3)
              ENDIF
              X1=X1+DDX
              X2=X2+DDX
              COUN=COUN+DCOUN
20        CONTINUE
      ENDIF
C------------------------------------------------------------------
C
C CRTANJE GRAFIKA U LOG10 RAZMERI PO Y-OSI
C
      YMAX=FLOAT(IYMAX)
      YMIN=FLOAT(IYMIN)
C
      X=(SNGL(XVEC(1))-XMIN)*XX/(XMAX-XMIN)+XSTART
      Y=ALOG(SNGL(YVEC(1)))/AL10
      Y=(Y-YMIN)*YY/(YMAX-YMIN)+YSTART
      IF(INDEX.NE.4)CALL PLOT(X,Y,3)
      XOL=X
      YOL=Y
C
      DO 30 I=2,IDIM
         X=(SNGL(XVEC(I))-XMIN)*XX/(XMAX-XMIN)+XSTART
         Y=ALOG(SNGL(YVEC(I)))/AL10
         Y=(Y-YMIN)*YY/(YMAX-YMIN)+YSTART
         XNE=X
         YNE=Y
         IF(INDEX.EQ.4)THEN
            CALL BOLLIN(XOL,YOL,XNE,YNE,ICOL,ISTY)
            XOL=XNE
            YOL=YNE
         ELSE
            CALL PLOT(X,Y,2)
         ENDIF
30    CONTINUE
C
      IF(INDEX.EQ.2)THEN
          CALL PLOT(0.,0.,999)
      ELSE IF(INDEX.EQ.4)THEN
         CALL CLOSEM(IOSTAT)
         IF(IOSTAT.NE.0)THEN
            PRINT *,'** GRAFRS: GRESKA PRI ZATVARANJU BAZE PODATAKA !'
            GOTO 99
         ENDIF
      ENDIF
C
99    RETURN
      END
