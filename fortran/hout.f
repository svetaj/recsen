      FUNCTION HOUT(F)
C=============================================================
C FUNKCIJA  : KOSINUS-KVADRATNA TRANSFER FUNKCIJA
C             
C  ULAZNE PROMENLJIVE:
C
C   F      = FREKVENCIJA  /0-1/
C   FB     = BINARNI PROTOK
C
C OSTALE PROMENLJIVE:
C
C   BETA   = FAKTOR KOSINUS-KVADRATNE FUNKCIJE
C============================================================
C
      INCLUDE 'FIBER.COM'
C
      DOUBLE COMPLEX HOUT
      DATA PI/3.1415926/
C
C IZRACUNAVANJE FUNKCIJE
C
      IF(DABS(F).LT.(1.-BETA)/2.)THEN
          HOUT=DCMPLX(1.,0.)
      ELSE IF(DABS(F).GE.(1.-BETA)/2..AND.DABS(F).LT.(1.+BETA)/2.)THEN
          HOUT=DCMPLX((1.-DSIN(PI*F/BETA-PI/(2.*BETA)))/2.,0.)
      ELSE
          HOUT=DCMPLX(0.,0.)
      ENDIF
C
      RETURN
      END
