C===============================================================
      SUBROUTINE GRAFIK(VECTOR,IDIM)
C---------------------------------------------------------------
C
      DIMENSION VECTOR(*)
      DOUBLE PRECISION VECTOR
      CHARACTER DUMMY,IMEM*12
C
1000  FORMAT(A1)
C
      ICOL=1
      ICOUN=ICOUN+1
      IF(ICOUN.GT.2)THEN
         IF(INDEX.EQ.1)THEN
             PRINT *,' PRITISNI <RETURN> ZA NASTAVAK'
             READ(5,1000)DUMMY
             CALL LLPAGE
         ENDIF
         IF(INDEX.EQ.2)THEN
             PRINT *,' PROMENI PAPIR !'
             PRINT *,' PRITISNI <RETURN> ZA NASTAVAK'
             READ(5,1000)DUMMY
         ENDIF
         ICOUN=1
      ENDIF
C
      PRINT *,' EKRAN/PLOTER/PRINTER/MAKRO/NISTA (1/2/3/4/5)?'
      READ *,INDEX
C
      IF(INDEX.EQ.1)THEN
         XSTART=10.
         YSTART=15.
         IF(ICOUN.EQ.2)YSTART=YSTART-12.
         CALL GOSEL(10)
      ELSE IF(INDEX.EQ.2)THEN
C------------------------------
C!       ZA A0 PLOTER         !
C!       XSTART=-0.3          !
C------------------------------
C!       ZA A3 PLOTER         !
         XSTART=3.3           
C------------------------------
         YSTART=15.5
         IF(ICOUN.EQ.2)YSTART=YSTART-13.
         CALL GOSEL(11)
      ELSE IF(INDEX.EQ.3)THEN
         WRITE(6,*)(VECTOR(J),J=1,IDIM)
      ELSE IF(INDEX.EQ.4)THEN
         XSTART=4.
         YSTART=6.7
         PRINT *,' UNESI IME MAKROA'
         READ *,IMEM
         CALL OPENM(IMEM,IOSTAT)
         IF(IOSTAT.NE.0)THEN
            PRINT *,'** GRAFIK: GRESKA PRI OTVARANJU 2D BAZE PODATAKA !'
            GOTO 99
         ENDIF
      ELSE
         GOTO 99
      ENDIF
C
      CALL PLOTSE(0,0,11)
C
      DELTAX=0.5
      XX=13.
      YY=8.
      XMIN=0.
      XMAX=(IDIM-1)*DELTAX
      X1=0.
C
      IF(INDEX.EQ.4)THEN
         CALL BOLLIN(XSTART,YSTART,XSTART+XX,YSTART,ICOL,0)
         CALL BOLLIN(XSTART+XX,YSTART,XSTART+XX,YSTART+YY,ICOL,0)
         CALL BOLLIN(XSTART+XX,YSTART+YY,XSTART,YSTART+YY,ICOL,0)
         CALL BOLLIN(XSTART,YSTART+YY,XSTART,YSTART,ICOL,0)
      ELSE
         CALL PLOT(XSTART,YSTART,3)
         CALL PLOT(XSTART+XX,YSTART,2)
         CALL PLOT(XSTART+XX,YSTART+YY,2)
         CALL PLOT(XSTART,YSTART+YY,2)
         CALL PLOT(XSTART,YSTART,2)
      ENDIF
C
      YMAX=SNGL(VECTOR(1))
      YMIN=SNGL(VECTOR(1))
      DO 10 I=2,IDIM
         IF(SNGL(VECTOR(I)).GT.YMAX)YMAX=SNGL(VECTOR(I))
         IF(SNGL(VECTOR(I)).LT.YMIN)YMIN=SNGL(VECTOR(I))
10    CONTINUE
      PRINT *,' ***** YMAX,YMIN=',YMAX,YMIN,' *****'
      IF(ABS(YMAX-YMIN).LT.1.E-7)GOTO 99
C
      IF(YMAX.GT.0.AND.YMIN.LT.0)THEN
         Y=-YMIN*YY/(YMAX-YMIN)+YSTART
         CALL PLOT(XSTART,Y,3)
         CALL PLOT(XSTART+XX,Y,2)
      ENDIF
C
      IF(XMAX.GT.0.AND.XMIN.LT.0)THEN
         X=-XMIN*XX/(XMAX-XMIN)+XSTART
         CALL PLOT(X,YSTART,3)
         CALL PLOT(X,YSTART+YY,2)
      ENDIF
C
      Y=(SNGL(VECTOR(1))-YMIN)*YY/(YMAX-YMIN)+YSTART
      IF(INDEX.NE.4)CALL PLOT(XSTART,Y,3)
      XOL=XSTART
      YOL=Y
C
      DO 20 I=2,IDIM
         X1=X1+DELTAX
         X=(X1-XMIN)*XX/(XMAX-XMIN)+XSTART
         Y=(SNGL(VECTOR(I))-YMIN)*YY/(YMAX-YMIN)+YSTART
         IF(INDEX.EQ.4)THEN
            XNE=X
            YNE=Y
            CALL BOLLIN(XOL,YOL,XNE,YNE,ICOL,0)
            XOL=XNE
            YOL=YNE
         ELSE
            CALL PLOT(X,Y,2)
         ENDIF
20    CONTINUE
C
      IF(INDEX.EQ.2)CALL PLOT(0.,0.,999)
      IF(INDEX.EQ.4)CALL CLOSEM(IOSTAT)
C
99    RETURN
      END
