      SUBROUTINE PTEST
C===============================================================
C===============================================================
C
      INCLUDE 'FIBER.COM'
C
      PRINT *,' ******* PSR **********'
      DO 10 I=-NMOD,NMOD
          PRINT *,I,PSR(I)
10    CONTINUE
C
      PRINT *,' ******* P2SR **********'
      DO 20 I=-NMOD,NMOD
          DO 30 J=-NMOD,NMOD
               PRINT *,I,J,P2SR(I,J)
30        CONTINUE
20    CONTINUE
C
      PRINT *,'######################################################'
      DO 50 K=-NMOD,NMOD,1
         DO 40 L=-NMOD,NMOD,1
             TEMP=P2SR(K,L)+(PT2SR(K,L)-PTSR2(K,L)-1.)*PSR(K)*PSR(L)
             PRINT *,'K,L,P=',K,L,TEMP
40        CONTINUE
50    CONTINUE
      PRINT *,'######################################################'
C
C
      RETURN
      END
