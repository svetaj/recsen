      SUBROUTINE WSINI(INDN)
C======================================================================
C  PODPROGRAM  = NALAZI ZA DATO I INFORMACIONI SADRZAJ KOJI DAJE 
C                NAJGORU VREDNOST SUMA ZA DATI LINIJSKI KOD
C
C  ULAZNE PROMENLJIVE:
C
C   INDN   = INDIKATOR ZA KOJI SE SUM TRAZI NAJGORA SEKVENCA
C            1 - SUM PRERASPODELE
C            2 - SACMA SUM
C
C  IZLAZ: 
C
C   FIBER  = KOMON U KOJI SE UPISUJU REZULTATI
C
C  OPIS PROMENLJIVIH:
C
C   MC     = ZA mBnB LINIJSKI KOD,  MC=m
C   NC     = ZA mBnB LINIJSKI KOD,  NC=n
C   NMD    = BROJ MODOVA mBnB LINIJSKOG KODA
C   IT     = KODNA TABELA ZA MOD #1 I MOD #2
C   ITN    = TABELA KOJA SADRZI INFORMACIJU KOJI JE SLEDECI MOD
C   INI    = INDIKATOR DA LI JE INCIJALIZOVANA TABELA ZA NAJGORI
C            SLUCAJ ZA DATI MOD
C   ICENT  = TABELA KOJA SADRZI NAJGORU SREDNJU REC ZA "1" I "0"
C   IPLUS  = TABELA KOJA SADRZI SLEDECE DVE NAGORE RECI ZA "1" I "0"
C   IMINUS = TABELA KOJA SADRZI PRETHODNE DVE NAGORE RECI ZA "1" I "0"
C   IC     = OFFSET KOJI POKAZUJE POZICIJU t=0 ZA "1" I "0"
C
C  POZIVI PODPROGRAMA:
C
C   RDSE   = PROSIRUJE OPSEG ZA ISTART IZRACUNATIH INTEGRALA
C======================================================================
C
      INCLUDE 'FIBER.COM'
      CHARACTER STR*2,FORM*6,FMT*80,FMT1*10
C
C INICIJALIZACIJA
C
      NMX=ISTART/NC+1
C
      DO 10 IB=1,2
C
C PRONALAZENJE NAJGORE CENTRALNE RECI 
C
          IC(INDN,IB)=1
          MODE=1
          SMAX=0.
          IWCENT=1
          DO 20 ICT=1,NC
              DO 30 JJ=1,NMD
                  DO 40 KK=1,2**MC
                      IF(IT(JJ,ICT,KK).NE.IB-1)GOTO 40
                      SUM=0.
                      DO 50 I1=ICT-1,1,-1
                          SUM=SUM+IT(JJ,I1,KK)*RDSE(INDN,I1-ICT)
50                    CONTINUE
C
                      DO 60 I1=ICT+1,NC
                          SUM=SUM+IT(JJ,I1,KK)*RDSE(INDN,I1-ICT)
60                    CONTINUE
C
                      IF(SUM.GT.SMAX)THEN
                          PRINT *, '#1 ', KK, SUM, SMAX
                          SMAX=SUM
                          IWCENT=KK
                          MODE=JJ
                          IC(INDN,IB)=ICT
                          DO 70 LL=1,NC
                              ICENT(INDN,IB,LL)=IT(JJ,LL,KK)
70                        CONTINUE
                      ENDIF
40                CONTINUE
30            CONTINUE
20        CONTINUE
C
C PRONALAZENJE SLEDECIH NMX NAJGORIH RECI
C
          MODE1=ITN(MODE,IWCENT)
          IW=IWCENT
          DO 80 JJ=1,NMX
              NMODE=MODE1
              SMAX=0.
              DO 90 KK=1,2**MC
                  SUM=0.
                  DO 100 I1=1,NC
                      TEMP=RDSE(INDN,I1+JJ*NC-IC(INDN,IB))
C                      TEMP=RDSE(INDN,I1+(JJ-1)*NC-IC(INDN,IB))
                      SUM=SUM+IT(NMODE,I1,KK)*TEMP
100               CONTINUE
C
                  IF(SUM.GT.SMAX)THEN
                      PRINT *, '#2 ', JJ, KK, SUM, SMAX
                      SMAX=SUM
                      IW=KK
                      MODE1=ITN(NMODE,IW)
                      DO 110 LL=1,NC
                          IPLUS(INDN,IB,JJ,LL)=IT(NMODE,LL,KK)
110                   CONTINUE
                  ENDIF
90            CONTINUE
80        CONTINUE
C
C PRONALAZENJE PRETHODNIH NMX NAJGORIH RECI
C
          MODE1=MODE
          IW=IWCENT
          DO 120 JJ=1,NMX
              NMODE=MODE1
              SMAX=0.
              DO 130 JJJ=1,NMD
                  DO 140 KK=1,2**MC
                      IF(ITN(JJJ,KK).NE.NMODE)GOTO 140
                      SUM=0.
                      DO 150 I1=NC,1,-1
                          TEMP=RDSE(INDN,I1-JJ*NC-IC(INDN,IB))
C                          TEMP=RDSE(INDN,I1+(JJ-1)*NC-IC(INDN,IB))
                          SUM=SUM+IT(JJJ,I1,KK)*TEMP
150                   CONTINUE
C
                      IF(SUM.GT.SMAX)THEN
                          PRINT *, '#3 ', JJ, KK, SUM, SMAX
                          SMAX=SUM
                          IW=KK
                          MODE1=JJJ
                          DO 160 LL=1,NC
                              IMINUS(INDN,IB,JJ,LL)=IT(JJJ,LL,KK)
160                       CONTINUE
                      ENDIF
140               CONTINUE
130           CONTINUE
120       CONTINUE
10    CONTINUE
C
      IF(INDN.EQ.1)THEN
         PRINT *,'            *** SUM PRERASPODELE ***'
      ELSE
         PRINT *,'                *** SACMA SUM ***'
      ENDIF
C
C STAMPANJE REZULTATA ZA PRIMLJENO "0" U t=0
C
      CALL INTCHR(NC,STR,1)
      FORM=STR//'I1'
      FMT='(12X,'//FORM//',A1,'//FORM//',A1,'//FORM//',A1,'//FORM//
     *    ',A1,'//FORM//')'
C
      PRINT *,'        ***************************************'
      PRINT *,'        * NAJGORE SEKVENCE SA STANOVISTA SUMA *'
      PRINT *,'        ***************************************'
      PRINT *,'                   ****** "0" ******'
      WRITE(6,FMT)(IMINUS(INDN,1,2,I),I=1,NC),'|',
     *            (IMINUS(INDN,1,1,I),I=1,NC),'|',
     *            (ICENT(INDN,1,I),I=1,NC),'|',
     *            (IPLUS(INDN,1,1,I),I=1,NC),'|',
     *            (IPLUS(INDN,1,2,I),I=1,NC)
      ITTT=2*NC+IC(INDN,1)+1+12
      CALL INTCHR(ITTT,STR,1)
      FMT1='('//STR//'X,A1)'
      WRITE(6,FMT1)'*'
      PRINT *,'IC(',INDN,',1)=',IC(INDN,1)
C
C STAMPANJE REZULTATA ZA PRIMLJENO "1" U t=0
C
      PRINT *,'                   ****** "1" ******'
      WRITE(6,FMT)(IMINUS(INDN,2,2,I),I=1,NC),'|',
     *            (IMINUS(INDN,2,1,I),I=1,NC),'|',
     *            (ICENT(INDN,2,I),I=1,NC),'|',
     *            (IPLUS(INDN,2,1,I),I=1,NC),'|',
     *            (IPLUS(INDN,2,2,I),I=1,NC)
      ITTT=2*NC+IC(INDN,2)+1+12
      CALL INTCHR(ITTT,STR,1)
      FMT1='('//STR//'X,A1)'
      WRITE(6,FMT1)'*'
      PRINT *,'IC(',INDN,',2)=',IC(INDN,2)
C
      RETURN
      END
