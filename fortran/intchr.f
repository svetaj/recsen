      SUBROUTINE INTCHR(INT,STRING,INDEX)
C==============================================================
C PURPOSE: CONVERT INTEGER CONSTANT TO A CHARACTER STRING
C          IF THE INTEGER IS LONGER THEN STRING, RETURNS
C          STRING FILLED WITH BLANKS
C
C INPUT LIST: 
C
C    INT     = INTEGER CONSTANT
C    INDEX   = 0  --> RIGHT JUSTIFIED WITH LEADING ZEROS
C              1  --> RIGHT JUSTIFIED WITH LEADING BLANKS
C              2  --> LEFT JUSTIFIED WITH TRAILING BLANKS
C
C OUTPUT LIST:
C
C    STRING  = CONVERTED INTEGER
C==============================================================
C
      CHARACTER*(*) STRING
      LOGICAL FIRST
C----------------------------- INITIALIZE AUTOMATIC CONVERSION
      FIRST=.FALSE.
      ILEN=LEN(STRING)
      STRING=' '
      IX=ILEN
      IY=INT
      ICOUN=0
C----------------------------- SPECIAL TREATMENT OF ZERO------
      IF(INT.EQ.0) THEN
         IF(INDEX.EQ.0) THEN
            DO 1 II=1,ILEN
               STRING(II:II)='0'
1           CONTINUE
         ELSE IF (INDEX.EQ.1) THEN
            STRING(ILEN:ILEN)='0'
         ELSE
            STRING(1:1)='0'
         ENDIF
         GOTO 99
      ENDIF
C
10    CONTINUE
C--------------------- SPLIT INTEGER /IOLD/ INTO DIGITS /INEW/
          IX=IX-1
          IF(IX.LT.0)GOTO 99
          IPOV=10**IX
          IOLD=IY
          IY=IY/IPOV
          IZ=IOLD-IY*IPOV
          INEW=IY
C-------------------------- IS IT INTEGER LONGER THEN STRING ?
          IF(INEW.GT.9)THEN
               STRING=' '
               GOTO 99
          ENDIF
C------------------------- DEAL WITH LEADING BLANKS (OR ZEROS)
          IF(.NOT.FIRST)THEN
              IF(INEW.NE.0)THEN
                  FIRST=.TRUE.
              ELSE
                  IF(INDEX.NE.0) INEW=-16
                  IF(INDEX.EQ.2)ICOUN=ICOUN+1
              ENDIF
          ENDIF
C------------------------------------- FILL STRING WITH DIGITS 
          IY=IZ
          II=ILEN-IX-ICOUN
          IF(II.LT.1)GOTO 10
          STRING(II:II)=CHAR(INEW+48)
      GOTO 10
C
99    RETURN
      END
