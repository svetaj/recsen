      PROGRAM C8B10B
C===================================================================
C GLAVNI PROGRAM = GENERISE TABELU 8B10B KODA . REZULTAT SE STAMPA
C                  NA stdio. 8B10B = 5B6B + 3B4B
C===================================================================
C
      CHARACTER*6 IC1(2,32)
      CHARACTER*4 IC2(2,8)
      INTEGER NM1(2,32),NM2(2,8)
C
1000  FORMAT(T5,A6,A4,T20,I1,T30,A6,A4,T45,I1)
C
C  /+/ 5B6B MOD
C
      IC1(1,1)='011000'
      IC1(1,2)='100010'
      IC1(1,3)='010010'
      IC1(1,4)='110001'
      IC1(1,5)='001010'
      IC1(1,6)='101001'
      IC1(1,7)='011001'
      IC1(1,8)='000111'
      IC1(1,9)='000110'
      IC1(1,10)='100101'
      IC1(1,11)='010101'
      IC1(1,12)='110100'
      IC1(1,13)='001101'
      IC1(1,14)='101100'
      IC1(1,15)='011100'
      IC1(1,16)='101000'
      IC1(1,17)='100100'
      IC1(1,18)='100011'
      IC1(1,19)='010011'
      IC1(1,20)='110010'
      IC1(1,21)='001011'
      IC1(1,22)='101010'
      IC1(1,23)='011010'
      IC1(1,24)='000101'
      IC1(1,25)='001100'
      IC1(1,26)='100110'
      IC1(1,27)='010110'
      IC1(1,28)='001001'
      IC1(1,29)='001110'
      IC1(1,30)='010001'
      IC1(1,31)='100001'
      IC1(1,32)='010100'
C
C  /-/ 5B6B MOD
C
      IC1(2,1)='100111'
      IC1(2,2)='011101'
      IC1(2,3)='101101'
      IC1(2,4)='110001'
      IC1(2,5)='110101'
      IC1(2,6)='101001'
      IC1(2,7)='011001'
      IC1(2,8)='111000'
      IC1(2,9)='111001'
      IC1(2,10)='100101'
      IC1(2,11)='010101'
      IC1(2,12)='110100'
      IC1(2,13)='001101'
      IC1(2,14)='101100'
      IC1(2,15)='011100'
      IC1(2,16)='010111'
      IC1(2,17)='100100'
      IC1(2,18)='100011'
      IC1(2,19)='010011'
      IC1(2,20)='110010'
      IC1(2,21)='001011'
      IC1(2,22)='101010'
      IC1(2,23)='011010'
      IC1(2,24)='111010'
      IC1(2,25)='110011'
      IC1(2,26)='100110'
      IC1(2,27)='010110'
      IC1(2,28)='110110'
      IC1(2,29)='001110'
      IC1(2,30)='101110'
      IC1(2,31)='011110'
      IC1(2,32)='101011'
C 
C ZA /+/ 5B6B MOD , SLEDECI MOD
C
      NM1(1,1)=2
      NM1(1,2)=2
      NM1(1,3)=2
      NM1(1,4)=1
      NM1(1,5)=2
      NM1(1,6)=1
      NM1(1,7)=1
      NM1(1,8)=1
      NM1(1,9)=2
      NM1(1,10)=1
      NM1(1,11)=1
      NM1(1,12)=1
      NM1(1,13)=1
      NM1(1,14)=1
      NM1(1,15)=1
      NM1(1,16)=2
      NM1(1,17)=2
      NM1(1,18)=1
      NM1(1,19)=1
      NM1(1,20)=1
      NM1(1,21)=1
      NM1(1,22)=1
      NM1(1,23)=1
      NM1(1,24)=2
      NM1(1,25)=2
      NM1(1,26)=1
      NM1(1,27)=1
      NM1(1,28)=2
      NM1(1,29)=1
      NM1(1,30)=2
      NM1(1,31)=2
      NM1(1,32)=2
C 
C ZA /-/ 5B6B MOD , SLEDECI MOD
C
      NM1(2,1)=1
      NM1(2,2)=1
      NM1(2,3)=1
      NM1(2,4)=2
      NM1(2,5)=1
      NM1(2,6)=2
      NM1(2,7)=2
      NM1(2,8)=1
      NM1(2,9)=1
      NM1(2,10)=2
      NM1(2,11)=2
      NM1(2,12)=2
      NM1(2,13)=2
      NM1(2,14)=2
      NM1(2,15)=2
      NM1(2,16)=1
      NM1(2,17)=1
      NM1(2,18)=2
      NM1(2,19)=2
      NM1(2,20)=2
      NM1(2,21)=2
      NM1(2,22)=2
      NM1(2,23)=2
      NM1(2,24)=1
      NM1(2,25)=1
      NM1(2,26)=2
      NM1(2,27)=2
      NM1(2,28)=1
      NM1(2,29)=2
      NM1(2,30)=1
      NM1(2,31)=1
      NM1(2,32)=1
C
C  /+/ 3B4B MOD 
C
      IC2(1,1)='0100'
      IC2(1,2)='1001'
      IC2(1,3)='0101'
      IC2(1,4)='0011'
      IC2(1,5)='0010'
      IC2(1,6)='1010'
      IC2(1,7)='0110'
      IC2(1,8)='0001'
C
C  /-/ 3B4B MOD 
C
      IC2(2,1)='1011'
      IC2(2,2)='1001'
      IC2(2,3)='0101'
      IC2(2,4)='0011'
      IC2(2,5)='1101'
      IC2(2,6)='1010'
      IC2(2,7)='0110'
      IC2(2,8)='1110'
C
C ZA /+/ 3B4B MOD , SLEDECI MOD
C
      NM2(1,1)=2
      NM2(1,2)=1
      NM2(1,3)=1
      NM2(1,4)=2
      NM2(1,5)=2
      NM2(1,6)=1
      NM2(1,7)=1
      NM2(1,8)=2
C
C ZA /-/ 3B4B MOD , SLEDECI MOD
C
      NM2(2,1)=1
      NM2(2,2)=2
      NM2(2,3)=2
      NM2(2,4)=1
      NM2(2,5)=1
      NM2(2,6)=2
      NM2(2,7)=2
      NM2(2,8)=1
C
C 8B10B = 5B6B + 3B4B
C
      DO 10 I=1,32
          DO 20 J=1,8
              WRITE(6,1000)IC1(1,I),IC2(NM1(1,I),J),NM2(NM1(1,I),J),
     *                     IC1(2,I),IC2(NM1(2,I),J),NM2(NM1(2,I),J)
20        CONTINUE
10    CONTINUE
C
      STOP
      END
