      SUBROUTINE GRAFWS(INDN,M,N,WS,NWS)
C==================================================================
C  PODPROGRAM :  CRTA NAJGORU SEKVENCU SIMBOLA U POGLEDU SUMA
C                CRTEZ SE SMESTA U MAKRO BAZU PODATAKA
C
C    ULAZNE PROMENLJIVE:
C
C  INDN   = INDIKATOR DA LI SE CRTA REALNA POVORKA (0)
C           ILI INFORMACIONI SADRZAJ (1)
C  M,N    = KOD JE mBnB
C  WS     = VEKTOR KOJI SADRZI NAJGORU SEKVENCU
C  NWS    = BROJ SIMBOLA , DIMENZIJA IWS
C==================================================================
C 
      DOUBLE PRECISION WS(*)
      CHARACTER*12 IME
      DATA III/4H****/
C
C INICIJALIZACIJA PARAMETARA CRTEZA
C
      X=0.
      Y=0.
      DX=0.5*10./8.*FLOAT(M)/FLOAT(N)
      DY=50.
      FACY=0.002
      IF(INDN.EQ.1)THEN
         DY=1.
         FACY=0.1
      ENDIF
C
      PRINT *,'EKRAN/MAKRO/NISTA (1/2/3) ?'
      READ *,IZZ
      IF(IZZ.EQ.1)THEN
         CALL PLOTSE(0,0,11)
         CALL PPLNIN(1)
         CALL PPLNST(0)
      ELSE IF(IZZ.EQ.2)THEN
C
C OTVARANJE MAKROA
C
         PRINT *,'UNESITE IME MAKROA'
         READ *,IME
         CALL OPENM(IME,IST)
         IF(IST.NE.0)GOTO 99
      ELSE
         GOTO 99
      ENDIF
      CALL GRFRST
C
C UPISIVANJE CRTEZA U MAKRO
C
      DO 10 J=1,NWS
          BR=WS(J)
          IF(INDN.EQ.1)THEN
             IF(BR.NE.0.)BR=1.
          ENDIF
          X1=X
          X2=X+DX
          Y1=DY*BR
          IF(IZZ.EQ.1)THEN
             CALL PLOT(X1,Y1,3)
             CALL PLOT(X2,Y1,2)
             CALL PLOT(X,Y,3)
             CALL PLOT(X1,Y1,2)
             CALL PLOT(X1,0.,3)
             CALL PLOT(X1,FACY*DY,2)
          ELSE IF(IZZ.EQ.2)THEN
             CALL BOLLIN(X1,Y1,X2,Y1,1,0)
             CALL BOLLIN(X,Y,X1,Y1,1,0)
             CALL BOLLIN(X1,0.,X1,FACY*DY,1,0)
          ENDIF
          IND=0
          Y=Y1
          X=X+DX
10    CONTINUE
C
C ZATVARANJE MAKROA
C
      IF(IZZ.EQ.1)THEN
         CALL PLOT(0.,0.,999)
      ELSE IF(IZZ.EQ.2)THEN
         CALL CLOSEM(IST)
      ENDIF
C
99    RETURN
      END
