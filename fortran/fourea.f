C=============================================================
      SUBROUTINE FOUREA(DATA,N,ISI)
C-------------------------------------------------------------
C PODPROGRAM: COMPLEX COOLEY-TUKEU FAST FOURIER TRANSFORM
C
C ULAZNE PROMENLJIVE:
C
C  DATA   = KOMPLEKSNI PODACI
C  N      = DIMENZIJA DATA VEKTORA JE 2**N
C  ISI    = +1 --> INVERZNA TRANSFORMACIJA
C           -1 --> DIREKTNA TRANSFORMACIJA
C
C IZLAZNE PROMENLJIVE:
C
C  DATA   = REZULTAT FFT
C=============================================================
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE COMPLEX DATA(*),TEMP,W
C
C POTVRDA STEPENA 2 DO 15
C
      NN=1
      DO 10 I=1,15
          M=I
          NN=NN*2
          IF(NN.EQ.N)GO TO 20
10    CONTINUE
      PRINT *,'*** FOUREA: N NIJE STEPEN 2 !'
      STOP
C
20    CONTINUE
C
      PI=DBLE(4.)*DATAN(DBLE(1.))
      FN=N
C
C STAVLJANJE PODATAKA U INVERTOVANI RED
C
      J=1
      DO 80 I=1,N
C
C I i J SU INVERTOVANI PAR
C
          IF(I-J) 30,40,40
C
C ZAMENA DATA(I) SA DATA(J) AKO JE I < J
C
30        TEMP=DATA(J)
          DATA(J)=DATA(I)
          DATA(I)=TEMP
C
C INVERZNI BROJAC
C
40        M=N/2
50        IF(J-M) 70,70,60
60           J=J-M
             M=(M+1)/2
          GOTO 50
C
70        J=J+M
C
80    CONTINUE
C
C RACUNANJE LEPTIROVA
C
      MMAX=1
C
90    IF(MMAX-N) 100,130,130
C
100      ISTEP=2*MMAX
         DO 120 M=1,MMAX
             THETA=PI*DBLE(FLOAT(ISI*(M-1))/FLOAT(MMAX))
             W=DCMPLX(DCOS(THETA),DSIN(THETA))
C
             DO 110 I=M,N,ISTEP
                  J=I+MMAX
                  TEMP=W*DATA(J)
                  DATA(J)=DATA(I)-TEMP
                  DATA(I)=DATA(I)+TEMP
110          CONTINUE
120      CONTINUE
C
         MMAX=ISTEP
      GOTO 90
C
C ZA INVERZNU TRANSF. --> IZLAZ=IZLAZ / N
C
130   IF(ISI.GE.0)THEN
          DO 150 I=1,N
              DATA(I)=DATA(I)/FN
150       CONTINUE
      ENDIF
C
      RETURN
      END
