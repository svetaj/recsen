      FUNCTION P2SR(KK,LL)
C=========================================================================
C   FUNKCIJA  = NALAZI SREDNJU VREDNOST ( % OD P_tot KOJI PRIPADA 
C               KK-TOM LONGITUDINALNOM MODU LD POMNOZENU SA % OD P_tot 
C               KOJI PRIPADA LL-TOM LONGITUDINALNOM MODU LD 
C
C   ULAZNE PROMENLJIVE:
C
C      KK    = REDNI BROJ LONGITUDINALNOG MODA LD  [-NMOD,NMOD]
C      LL    = REDNI BROJ LONGITUDINALNOG MODA LD  [-NMOD,NMOD]
C
C   OSTALE PROMENLJIVE:
C
C     CR     = BRZINA SPONTANE EMISIJE
C     AR     = BRZINA STIMULISANE EMISIJE
C     ISTM   = INDIKATOR IZRACUNAVANJA STATISTIKE SUMA PRERASPODELE
C
C   POZIVI FUNKCIJA:
C
C     PSR    = NALAZI SREDNJU VREDNOST ( % OD P_tot KOJI PRIPADA
C              K-TOM LONGITUDINALNOM MODU
C=========================================================================
C
      INCLUDE 'FIBER.COM'
C
      IF(ISTM.EQ.1)THEN
          P2SR=1.
      ELSE IF(ISTM.EQ.2)THEN
          FCT=CR/AR
          P2SR=PSR(KK)*PSR(LL)*FCT/(1.+FCT)
      ELSE IF(ISTM.EQ.3)THEN
C----------------------------------------------------------------------
C  PRETPOSTAVKA: SNAGE RAZLICITIH MODOVA SU STATISTICKI NEZAVISNE
C
C          P2SR=PSR(KK)*PSR(LL)
C----------------------------------------------------------------------
          FCT=CR/AR
          P2SR=PSR(KK)*PSR(LL)*FCT/(1.+FCT)
      ENDIF
C
      RETURN
      END
