C===============================================================
      SUBROUTINE CGRAF(CARRAY,IDIM)
C---------------------------------------------------------------
C
      COMPLEX*16 CARRAY(*)
      CHARACTER DUMMY
C
1000  FORMAT(A1)
C
      ICOUN=ICOUN+1
      IF(ICOUN.GT.2)THEN
         IF(INDEX.EQ.1)THEN
             PRINT *,' PRITISNI <RETURN> ZA NASTAVAK'
             READ(5,1000)DUMMY
             CALL LLPAGE
         ENDIF
         IF(INDEX.EQ.2)THEN
             PRINT *,' PROMENI PAPIR !'
             PRINT *,' PRITISNI <RETURN> ZA NASTAVAK'
             READ(5,1000)DUMMY
         ENDIF
         ICOUN=1
      ENDIF
C
      PRINT *,' EKRAN/PLOTER/PRINTER/NISTA (1/2/3/4)?'
      READ *,INDEX
C
      IF(INDEX.EQ.1)THEN
         XSTART=10.
         YSTART=15.
         IF(ICOUN.EQ.2)YSTART=YSTART-12.
         CALL GOSEL(10)
      ELSE IF(INDEX.EQ.2)THEN
C------------------------------
C!       ZA A0 PLOTER         !
C!       XSTART=-0.3          !
C------------------------------
C!       ZA A3 PLOTER         !
         XSTART=3.3           
C------------------------------
         YSTART=15.5
         IF(ICOUN.EQ.2)YSTART=YSTART-13.
         CALL GOSEL(11)
      ELSE
         IF(INDEX.EQ.3)WRITE(6,*)(CARRAY(J),J=1,IDIM)
         GOTO 99
      ENDIF
C
      CALL PLOTSE(0,0,11)
C
      DELTAX=0.5
      XX=13.
      YY=8.
      XMIN=0.
      XMAX=(IDIM-1)*DELTAX
      X1=0.
C
      CALL PLOT(XSTART,YSTART,3)
      CALL PLOT(XSTART+XX,YSTART,2)
      CALL PLOT(XSTART+XX,YSTART+YY,2)
      CALL PLOT(XSTART,YSTART+YY,2)
      CALL PLOT(XSTART,YSTART,2)
C
      YMAX=SNGL(CDABS(CARRAY(1)))
      YMIN=SNGL(CDABS(CARRAY(1)))
      DO 10 I=2,IDIM
         IF(SNGL(CDABS(CARRAY(I))).GT.YMAX)YMAX=SNGL(CDABS(CARRAY(I)))
         IF(SNGL(CDABS(CARRAY(I))).LT.YMIN)YMIN=SNGL(CDABS(CARRAY(I)))
10    CONTINUE
      PRINT *,' ***** YMAX,YMIN=',YMAX,YMIN,' *****'
      IF(ABS(YMAX-YMIN).LT.1.E-7)GOTO 99
C
      IF(YMAX.GT.0.AND.YMIN.LT.0)THEN
         Y=-YMIN*YY/(YMAX-YMIN)+YSTART
         CALL PLOT(XSTART,Y,3)
         CALL PLOT(XSTART+XX,Y,2)
      ENDIF
C
      IF(XMAX.GT.0.AND.XMIN.LT.0)THEN
         X=-XMIN*XX/(XMAX-XMIN)+XSTART
         CALL PLOT(X,YSTART,3)
         CALL PLOT(X,YSTART+YY,2)
      ENDIF
C
      Y=(SNGL(CDABS(CARRAY(1)))-YMIN)*YY/(YMAX-YMIN)+YSTART
      CALL PLOT(XSTART,Y,3)
      DO 20 I=2,IDIM
         X1=X1+DELTAX
         X=(X1-XMIN)*XX/(XMAX-XMIN)+XSTART
         Y=(SNGL(CDABS(CARRAY(I)))-YMIN)*YY/(YMAX-YMIN)+YSTART
         CALL PLOT(X,Y,2)
20    CONTINUE
      CALL PLOT(0.,0.,999)
C
99    RETURN
      END
