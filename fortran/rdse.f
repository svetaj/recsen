      FUNCTION RDSE(INDN,I)
C===============================================================
C FUNKCIJA  =  PROSIRUJE OPSEG IZRACUNATIH INTEGRALA RIKL I RIS
C              SA [-ISTART,ISTART] NA (-BESKONACNO,BESKONACNO)
C
C  ULAZNE PROMENLIVE:
C
C   INDN    = INDIKATOR KOJI JE SUM U PITANJU 
C             1 - PRERASPODELE
C             2 - SACMA SUM
C   I       = REDNI BROJ IMPULSA
C===============================================================
C
      INCLUDE 'FIBER.COM'
C
      IF(I.LT.-ISTART)THEN
           TEMP=RDS(INDN,1)
      ELSE IF(I.GT.ISTART)THEN
           TEMP=RDS(INDN,2*ISTART+1)
      ELSE
           TEMP=RDS(INDN,I+ISTART+1)
      ENDIF
C
      RDSE=TEMP
C
      RETURN
      END
