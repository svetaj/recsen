      PROGRAM RECSEN
C=====================================================================
C GLAVNI PROGRAM  = RACUNA OSETLJIVOST OPTICKOG PRIJEMNIKA ZA 
C                   RAZLICITE VREDNOSTI PARAMETARA SISTEMA ZA 
C                   DIGITALNI OPTICKI PRENOS VELIKIM BRZINAMA
C
C  OPIS PROMENLJIVIH:
C
C   FIBER     =  COMMON SA PARAMETRIMA SISTEMA ZA DIGITALNI OPTICKI
C                PRENOS, KAO I PARAMETRIMA POTREBNIM ZA NUMERICKO 
C                IZRACUNAVANJE SUMA
C
C  POZIVI PODPROGRAMA:
C
C   LOAPAR    =  UCITAVA PARAMETRE SA FAJLA
C   FIBINI    =  PODPROGRAM ZA INICIJALIZACIJU PARAMETARA
C   GRAFRS    =  CRTA ZAVISNOST VEROVATNOCE GRESKE OD PRIMLJENE
C                SREDNJE OPTICKE SNAGE
C   CHPAR     =  POJEDINACNO MENJANJE PARAMETARA
C   PARINI    =  INICIJALIZACIJA PARAMETARA
C   OPNTMP    =  OTVARA PRIVREMENE FAJLOVE
C   CLSTMP    =  ZATVARA PRIVREMENE FAJLOVE
C
C  POZIVI FUNKCIJA:
C
C   NWTOT     =  NALAZI UKUPNU SNAGU SUMA 
C=====================================================================
C
      INCLUDE 'FIBER.COM'
      DIMENSION XT(1000),YT(1000),TIME(2)
      DOUBLE PRECISION XT,YT
      CHARACTER IMEF*12,DANE*2,DUMMY*1
C
      PRINT *,'              ***************************************'
      PRINT *,'              *        R E C S E N    V 2.0         *'
      PRINT *,'              *    RECeiver SENsitivity Analysis    *'
      PRINT *,'              *-------------------------------------*'
      PRINT *,'              *    (C)opyright  SVETA 1989,1990.    *'
      PRINT *,'              *-------------------------------------*'
      PRINT *,'              *        DIGITAL FIBER OPTIC          *'
      PRINT *,'              *       COMMUNICATION SYSTEMS         *'
      PRINT *,'              ***************************************'
      PRINT *,' '
C
C INICIJALIZACIJA
C
      INDLD=0
      INDREZ=0
      SQ2=DSQRT(DBLE(2.))
      AL10=DLOG(DBLE(10.))
      IYMIN=-12
      IYMAX=-3
C
1     CONTINUE
C
      PRINT *,'            ********************************************'
      PRINT *,'            * 1. IZRACUNAVANJE OSETLJIVOSTI PRIJEMNIKA *'
      PRINT *,'            * 2. RUCNO UNOSENJE VREDNOSTI OSETLJIVOSTI *'
      PRINT *,'            * 3. FORMIRANJE DATOTEKE SA PARAMETRIMA    *'
      PRINT *,'            * 4. UCITAVANJE PARAMETARA IZ DATOTEKE     *'
      PRINT *,'            * 5. POJEDINACNO MENJANJE PARAMETARA       *'
      PRINT *,'            * 6. PREZENTACIJA REZULTATA (GRAFIK)       *'
      PRINT *,'            * 7. UPISIVANJE REZULTATA NA FAJL          *'
      PRINT *,'            * 8. KRAJ RADA                             *'
      PRINT *,'            *------------------------------------------*'
      PRINT *,'            *               UNESITE BROJ               *'
      PRINT *,'            ********************************************'
      READ *,IND
C
      GOTO(10,20,30,40,50,60,70,99),IND
C
C----------------------------------------------------------------------
C IZRACUNAVANJE VREDNOSTI SUMA
C
10    CONTINUE
      IF(INDLD.EQ.0)THEN
         PRINT *,' PRVO UCITAJTE PARAMETRE SA FAJLA !'
         GOTO 1
      ENDIF
C
C INICIJALIZACIJA PARAMETARA KOJI NISU PROMENLJIVI 
C
      CALL FIBINI
C
C RACUNANJE TACAKA GRAFIKA
C
      X1=XMIN
      DELTAX=(XMAX-XMIN)/DBLE(FLOAT(NPOI))
C
      DO 11 I=1,NPOI+1
         XT(I)=X1
         PX=0.001*DEXP(X1*AL10/10.)
C
C RACUNANJE NAJGORE VREDNOSTI UKUPNOG SUMA
C
         PNW=PNWTOT(PX)
         Q=1./PNW
         YT(I)=0.5*DERFC(Q/SQ2)
         X1=X1+DELTAX
11    CONTINUE
C
      NX=NPOI+1
      INDREZ=1
      GOTO 1
C
C----------------------------------------------------------------------
C RUCNO UNOSENJE OSETLJIVOSTI PRIJEMNIKA 
C
20    CONTINUE
C 
      PRINT *,' UNESITE BROJ TACAKA'
      READ *,NX
C
      DO 21 I=1,NX
          PRINT *,I,': UNESITE:   Psr [dBm] , Pe '
          READ *,XT(I),YT(I)
21    CONTINUE
C
      INDREZ=1
      GOTO 1
C
C----------------------------------------------------------------------
C FORMIRANJE DATOTEKE SA PARAMETRIMA  
C
30    CONTINUE
      CALL FIBPAR
      GOTO 1
C
C----------------------------------------------------------------------
C UCITAVANJE PARAMETARA IZ DATOTEKE
C
40    CONTINUE
      CALL LOAPAR
      INDLD=1
      GOTO 1
C
C----------------------------------------------------------------------
C POJEDINACNA KOREKCIJA PARAMETARA
C
50    CONTINUE
      IF(INDLD.EQ.0)THEN
         PRINT *,' PRVO UCITAJTE PARAMETRE SA FAJLA !'
         GOTO 1
      ENDIF
      CALL CHPAR
      GOTO 1
C
C----------------------------------------------------------------------
C CRTANJE GRAFIKA
C
60    CONTINUE
C
      PRINT *,'            ********************************************'
      PRINT *,'            * 1. CRTANJE POSLEDNJEG REZULTATA          *'
      PRINT *,'            * 2. CRTANJE REZULTATA IZ FAJLA            *'
      PRINT *,'            * 3. CRTANJE NAJGORE SEKVENCE SIMBOLA      *'
      PRINT *,'            * 4. STAMPANJE STATISTICKIH VREDNOSTI SUMA *'
      PRINT *,'            * 5. KRAJ FUNKCIJE                         *'
      PRINT *,'            *------------------------------------------*'
      PRINT *,'            *               UNESITE BROJ               *'
      PRINT *,'            ********************************************'
      READ *,INDC
C
      IF(INDC.EQ.1)THEN
          IF(INDREZ.EQ.0)GOTO 60
C
      ELSE IF(INDC.EQ.2)THEN
C
          PRINT *,' UNESITE IME FAJLA SA REZULTATIMA'
          READ *,IMEF
C
          OPEN(UNIT=10,FILE=IMEF,STATUS='OLD',IOSTAT=IST)
          IF(IST.NE.0)GOTO 60
C
          READ(10,*)NX
          READ(10,*)XMIN,XMAX
          DO 63 I=1,NX
              READ(10,*)XT(I),YT(I)
63        CONTINUE
          CLOSE(10)
C
          INDREZ=1
C
      ELSE IF(INDC.EQ.3)THEN
          PRINT *,' PRAVE VREDNOSTI/INFORMACIONI SADRZAJ (1/2) ?'
          READ *,IND1
          CALL WSTST(IND1-1,1)
          CALL WSTST(IND1-1,2)
          GOTO 60
      ELSE IF(INDC.EQ.4)THEN
          CALL PTEST
          GOTO 60
      ELSE IF(INDC.EQ.5)THEN
          GOTO 1
      ELSE
          GOTO 60
      ENDIF
C   
      PRINT *,' DA LI ZELITE CRTANJE OSA GRAFIKA DA/NE [1/0] ?'
      READ *,IM
      PRINT *,' STIL CRTANJA LINIJE [0-8] '
      READ *,IST
C      CALL GRAFRS(XT,YT,NX,IM,IST,IYMIN,IYMAX,SNGL(XMIN),SNGL(XMAX))
      GOTO 60
C
C----------------------------------------------------------------------
C
70    CONTINUE
      IF(INDREZ.EQ.0)GOTO 1
      PRINT *,' UNESITE IME FAJLA SA REZULTATIMA'
      READ *,IMEF
C
      OPEN(UNIT=10,FILE=IMEF,STATUS='NEW',IOSTAT=IST)
      IF(IST.NE.0)GOTO 1
C
      WRITE(10,*)NPOI+1
      WRITE(10,*)XMIN,XMAX
      DO 71 I=1,NPOI+1
         WRITE(10,*) XT(I),YT(I)
71    CONTINUE
C
      CLOSE(10)
      GOTO 1
C
C----------------------------------------------------------------------
C
99    PRINT *,' *** KRAJ PROGRAMA ***'
C
      STOP
      END
