      FUNCTION PPRIM(GX)
C================================================================
C FUNKCIJA : RACUNA VREDNOST PRVOG IZVODA SREDNJE PRIMLJENE 
C            OPTICKE SNAGE U TACKI GX
C
C  ULAZNE PROMENLJIVE:
C
C   GX      = VREDNOST POJACANJA APD
C================================================================
C
      INCLUDE 'FIBER.COM'
C
      G2X=DEXP((2.+FAC4(6))*DLOG(GX))
      CC1=(FAC4(6)*FAC4(2)*G2X-2.*FAC4(5))
      CC2=DSQRT(FAC4(2)*G2X+FAC4(4)*GX**2+FAC4(5))
      CC3=(FAC4(6)*FAC4(1)*G2X-2.*FAC4(5))
      CC4=DSQRT(FAC4(1)*G2X+FAC4(3)*GX**2+FAC4(5))
C
      PPRIM=CC1/CC2+CC3/CC4
C
      RETURN
      END
