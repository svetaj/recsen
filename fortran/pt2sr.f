      FUNCTION PT2SR(KK,LL)
C=========================================================================
C   FUNKCIJA   = NALAZI SREDNJU KVADRATNU VREDNOST PROMENE UKUPNE 
C                OPTICKE SNAGE LD
C
C  ULAZNE PROMENLJIVE:
C
C    KK    = REDNI BROJ LONGITUDINALNOG MODA LD  [-NMOD,NMOD]
C    LL    = REDNI BROJ LONGITUDINALNOG MODA LD  [-NMOD,NMOD]
C=========================================================================
C
      INCLUDE 'FIBER.COM'
C
      IF(ISTM.EQ.1)THEN
          PT2SR=1.
      ELSE IF(ISTM.EQ.2)THEN
          PT2SR=1.
      ELSE IF(ISTM.EQ.3)THEN
          PT2SR=SIGPT
      ENDIF
C
      RETURN
      END
