      FUNCTION PTSR2(KK,LL)
C=========================================================================
C   FUNKCIJA   = NALAZI KVADRAT SREDNJE VREDNOSTI PROMENE UKUPNE 
C                OPTICKE SNAGE LD
C
C  ULAZNE PROMENLJIVE:
C
C    KK    = REDNI BROJ LONGITUDINALNOG MODA LD  [-NMOD,NMOD]
C    LL    = REDNI BROJ LONGITUDINALNOG MODA LD  [-NMOD,NMOD]
C
C  OSTALE PROMENLJIVE:
C
C   ISTM   = INDIKATOR IZRACUNAVANJA STATISTIKE SUMA PRERASPODELE
C=========================================================================
C
      INCLUDE 'FIBER.COM'
C
      IF(ISTM.EQ.1)THEN
          PTSR2=1.
      ELSE IF(ISTM.EQ.2)THEN
          PTSR2=1.
      ELSE IF(ISTM.EQ.3)THEN
          PTSR2=SRVPT**2
      ENDIF
C
      RETURN
      END
