      SUBROUTINE WSTST(IND1,IND2)
C====================================================================
C PODPROGRAM : CRTA PRONADJENU NAJGORU SEKVENCU U POGLEDU SUMA
C              ZA DATI LINIJSKI KOD
C
C ULAZNE PROMENLJIVE:
C
C  IND1   = INDIKATOR DA LI SE CRTAJU STVARNE VREDNOSTI ODMERAKA (0)
C           ILI INFORMACIONI SADRZAJ (0)
C  IND2   = INDIKATOR ZA KOJI SE SUM TRAZI GRAFIK NAJGORE SEKVENCE
C           1 - SUM PRERASPODELE
C           2 - SACMA SUM
C====================================================================
C
      INCLUDE 'FIBER.COM'
      DIMENSION XII(100)
C
      IF(IND2.EQ.1)THEN
         PRINT *,'  *** SUM PRERASPODELE ***'
      ELSE
         PRINT *,'  *** SACMA SUM ***'
      ENDIF
C
C INICIJALIZACIJA PARAMETARA
C
      NSAMP=ISTART
C
      DO 20 IW=0,1
         PRINT *,' *** NAJGORA SEKVENCA ZA POSLATO : ',IW
         I=0
         DO 10 JJ=-NSAMP,NSAMP
            I=I+1
            XII(I)=WSAMP(IND2,JJ,IW)*RDSE(IND2,JJ)/RDSE(IND2,0)
10       CONTINUE
C         CALL GRAFWS(IND1,MC,NC,XII,2*NSAMP+1)
20    CONTINUE
C
      RETURN
      END
