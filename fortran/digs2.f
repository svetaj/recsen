      SUBROUTINE DIGS2(DS0,DS1)
C======================================================================
C PODPROGRAM  =  RACUNA DIGITALNE SUME KOJE ULAZE U SASTAV IZRAZA ZA 
C                NALAZENJE SACMA SUMA ZA PRIMLJENE IMPULSE "0" I "1"
C
C  IZLAZNE PROMENLJIVE:
C
C   DS0     = DIGITALNA SUMA ZA PRIMLJENO "0" U t=0
C   DS1     = DIGITALNA SUMA ZA PRIMLJENO "1" U t=0
C
C  OSTALE PROMENLJIVE:
C
C   ISTART  = UKUPAN BROJ IMPULSA KOJI ULAZE U PRORACUN JE 
C             2*ISTART+1
C
C  POZIVI FUNKCIJA:
C
C   WSAMP   = NALAZI ZA DATO I INFORMACIONI SADRZAJ KOJI DAJE 
C             NAJGORU VREDNOST SUMA ZA DATI LINIJSKI KOD
C   RIS     = RACUNA UTICAJ I-TOG IMPULSA NA VELICINU SACMA SUMA
C   WSINI   = NALAZI NAJGORU SEKVENCU SIMBOLA U POGLEDU SUMA
C======================================================================
C
      INCLUDE 'FIBER.COM'
      DOUBLE COMPLEX HOUT,HTF,HML
      EXTERNAL HOUT,HTF,HML
C
C INICIJALIZACIJA
C
      R20=0.
      R21=0.
C
C RACUNANJE INTEGRALA
C
      DO 10 I=-ISTART,ISTART
          RDS(2,I+ISTART+1)=RIS(HOUT,HTF,HML,I)
10    CONTINUE
C
C PRONALAZENJE NAJGORE SEKVENCE SIMBOLA ZA SLUCAJ SACMA SUMA
C
      CALL WSINI(2)
C
C RACUNANJE DIGITALNIH SUMA KOJE ULAZE U SASTAV IZRAZA ZA NALAZENJE 
C SACMA SUMA
C
      DO 40 I=-ISTART,ISTART,1
          R20=R20+WSAMP(2,I,0)*RDS(2,I+ISTART+1)
          R21=R21+WSAMP(2,I,1)*RDS(2,I+ISTART+1)
40    CONTINUE
C
      DS0=R20
      DS1=R21
C
      RETURN
      END
