      FUNCTION WSAMP(INDN,II,IBT0)
C======================================================================
C  FUNKCIJA  = NALAZI ZA DATO I INFORMACIONI SADRZAJ KOJI DAJE 
C              NAJGORU VREDNOST SUMA ZA DATI LINIJSKI KOD
C
C  ULAZNE PROMENLJIVE:
C
C   INDN   = INDIKATOR ZA KOJI SUM SE TRAZI NAJGORI INF. SADRZAJ
C            1 - SUM PRERASPODELE
C            2 - SACMA SUM
C   IBT0   = INFORMACIONI SADRZAJ KOJI JE PRIMLJEN U t=0
C   II     = REDNI BROJ IMPULSA ZA KOJI SE TRAZI NAJGORA VREDNOST 
C            INFORMACIONOG SADRZAJA
C
C  OSTALE PROMENLJIVE:
C
C   NC     = ZA mBnB LINIJSKI KOD,  NC=n
C   ICENT  = TABELA KOJA SADRZI NAJGORU SREDNJU REC ZA "1" I "0"
C   IPLUS  = TABELA KOJA SADRZI SLEDECE DVE NAGORE RECI ZA "1" I "0"
C   IMINUS = TABELA KOJA SADRZI PRETHODNE DVE NAGORE RECI ZA "1" I "0"
C   IC     = OFFSET KOJI POKAZUJE POZICIJU t=0 ZA "1" I "0"
C
C  NAPOMENA:
C
C   PRE PRVOG POZIVA OVE FUNKCIJE OBAVEZNO JE POZVATI PODPROGRAM
C   ZA INICIJALIZACIJU TABELA  WSINI
C======================================================================
C
      INCLUDE 'FIBER.COM'
C
C INICIJALIZACIJA
C
      IB0=IBT0+1
      WSAMP=0.
      IWSAMP=0.
      I=II
      NMX=ISTART/NC+1
      N=NC
      I=I+IC(INDN,IB0)
C
C PRONALAZENJE U VEC INICIJALIZOVANIM VEKTORIMA NAJGOREG INFOR-
C MACIONOG SADRZAJA ZA DATO I
C
      IF(I.GT.0.AND.I.LE.N)THEN
          IWSAMP=ICENT(INDN,IB0,I)
      ELSE IF(I.GT.N)THEN
          I=I-N
          JJ=(I-1)/N+1
          IF(JJ.GT.NMX)GOTO 99
          I=I-(JJ-1)*N
          IWSAMP=IPLUS(INDN,IB0,JJ,I)
      ELSE IF(I.LE.0)THEN
          I=ABS(I)+1
          JJ=(I-1)/N+1
          IF(JJ.GT.NMX)GOTO 99
          I=I-(JJ-1)*N
          I=N-I+1
          IWSAMP=IMINUS(INDN,IB0,JJ,I)
      ENDIF
C
      IF(IWSAMP.EQ.0)WSAMP=RO
      IF(IWSAMP.EQ.1)WSAMP=1.
C
99    RETURN
      END
