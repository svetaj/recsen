      SUBROUTINE LOAPAR
C========================================================================
C PODPROGRAM: UCITAVANJE PARAMETARA ZA IZRACUNAVANJE OSETLJIVOSTI 
C             OPTICKOG PRIJEMNIKA ZA RAZLICITE VREDNOSTI SA FAJLA
C========================================================================
C
      INCLUDE 'FIBER.COM'
      CHARACTER FNAME*12,STR*2,FORM*4,FFF*80
C
C  OTVARANJE FAJLA
C
      PRINT *,' UNESITE IME FAJLA SA PARAMETRIMA:'
      READ *,FNAME
C
      OPEN(UNIT=21,FILE=FNAME,STATUS='OLD',ACCESS='SEQUENTIAL',
     *     IOSTAT=IST)
      IF(IST.NE.0)THEN
          PRINT *,' GRESKA NA FAJLU: ',FNAME
          STOP
      ENDIF
C
C  UCITAVANJE PARAMETARA ZA SIMULACIJU
C
      READ(21,1000)NMOD
      READ(21,1001)NFFT
      IF(NFFT.GT.NFFMAX)NFFT=NFFMAX
      READ(21,1002)M
      READ(21,1003)A
      READ(21,1004)BETA
      READ(21,1005)SIGMA0
      READ(21,1006)ISTART
      IF(ISTART.GT.ISTMX1)ISTART=ISTMX1
      READ(21,1007)RO
      READ(21,1008)PTOT
      READ(21,1009)FB
      READ(21,1010)B1
      READ(21,1011)DM
      READ(21,1012)SIGMAI
      READ(21,1013)RL
      READ(21,1014)DLAMBD
      READ(21,1015)CLAMBD
      READ(21,1016)ETA
      READ(21,1017)GSR
      READ(21,1018)ALFA1
      READ(21,1019)RB
      READ(21,1020)CD
      READ(21,1021)DCURR
      READ(21,1022)BBIP
      READ(21,1023)RA
      READ(21,1024)CA
      READ(21,1025)CCURR
      READ(21,1026)X
      READ(21,1027)ISTM
      READ(21,1028)CR
      READ(21,1029)AR
      READ(21,1030)IGOPT
      READ(21,1031)ICOPT
      READ(21,1032)ICNTRL
      READ(21,1033)CCPERC
      READ(21,1034)GSPERC
      READ(21,1035)SIGPT
      READ(21,1036)SRVPT
      READ(21,1037)ALFAS
      READ(21,1038)XMIN
      READ(21,1039)XMAX
      READ(21,1040)NPOI
      READ(21,1041)MC
      READ(21,1042)NC
      READ(21,1043)NMD
      CALL INTCHR(NC,STR,1)
      FORM=STR//'I1'
      FFF='(T5,'//FORM//',T20,I1,T30,'//FORM//',T45,I1)'
      DO 100 I=1,2**MC
         READ(21,FFF)
     *       (IT(1,J,I),J=1,NC),ITN(1,I),(IT(2,J,I),J=1,NC),ITN(2,I)
100   CONTINUE
C
1000  FORMAT(//////,T56,I5)
1001  FORMAT(T56,I5)
1002  FORMAT(T56,I5)
1003  FORMAT(T51,E15.8)
1004  FORMAT(T51,E15.8)
1005  FORMAT(T51,E15.8)
1006  FORMAT(T56,I5)
1007  FORMAT(T51,E15.8)
1008  FORMAT(T51,E15.8)
1009  FORMAT(T51,E15.8)
1010  FORMAT(T51,E15.8)
1011  FORMAT(T51,E15.8)
1012  FORMAT(T51,E15.8)
1013  FORMAT(T51,E15.8)
1014  FORMAT(T51,E15.8)
1015  FORMAT(T51,E15.8)
1016  FORMAT(T51,E15.8)
1017  FORMAT(T51,E15.8)
1018  FORMAT(T51,E15.8)
1019  FORMAT(T51,E15.8)
1020  FORMAT(T51,E15.8)
1021  FORMAT(T51,E15.8)
1022  FORMAT(T51,E15.8)
1023  FORMAT(T51,E15.8)
1024  FORMAT(T51,E15.8)
1025  FORMAT(T51,E15.8)
1026  FORMAT(T51,E15.8)
1027  FORMAT(T56,I5)
1028  FORMAT(T51,E15.8)
1029  FORMAT(T51,E15.8)
1030  FORMAT(T56,I5)
1031  FORMAT(T56,I5)
1032  FORMAT(T56,I5)
1033  FORMAT(T51,E15.8)
1034  FORMAT(T51,E15.8)
1035  FORMAT(T51,E15.8)
1036  FORMAT(T51,E15.8)
1037  FORMAT(T51,E15.8)
1038  FORMAT(T51,E15.8)
1039  FORMAT(T51,E15.8)
1040  FORMAT(T56,I5)
1041  FORMAT(T56,I5)
1042  FORMAT(T56,I5)
1043  FORMAT(T56,I5)
C
C  ZATVARANJE FAJLA
C
      CLOSE(21)
C
C STAMPANJE UCITANIH PARAMETARA NA EKRANU
C
      WRITE(6,2000)NMOD
      WRITE(6,2001)NFFT
      WRITE(6,2002)M
      WRITE(6,2003)A
      WRITE(6,2004)BETA
      WRITE(6,2005)SIGMA0
      WRITE(6,2006)ISTART
      WRITE(6,2007)RO
      WRITE(6,2008)PTOT
      WRITE(6,2009)FB
      WRITE(6,2010)B1
      WRITE(6,2011)DM
      WRITE(6,2012)SIGMAI
      WRITE(6,2013)RL
      WRITE(6,2014)DLAMBD
      WRITE(6,2015)CLAMBD
      WRITE(6,2016)ETA
      WRITE(6,2017)GSR
      WRITE(6,2018)ALFA1
      WRITE(6,2019)RB
      WRITE(6,2020)CD
      WRITE(6,2021)DCURR
      WRITE(6,2022)BBIP
      WRITE(6,2023)RA
      WRITE(6,2024)CA
      WRITE(6,2025)CCURR
      WRITE(6,2026)X
      WRITE(6,2027)ISTM
      WRITE(6,2028)CR
      WRITE(6,2029)AR
      WRITE(6,2030)IGOPT
      WRITE(6,2031)ICOPT
      WRITE(6,2032)ICNTRL
      WRITE(6,2033)CCPERC
      WRITE(6,2034)GSPERC
      WRITE(6,2035)SIGPT
      WRITE(6,2036)SRVPT
      WRITE(6,2037)ALFAS
      WRITE(6,2038)XMIN
      WRITE(6,2039)XMAX
      WRITE(6,2040)NPOI 
      WRITE(6,2041)MC
      WRITE(6,2042)NC
      WRITE(6,2043)NMD
      DO 200 I=1,2**MC
         WRITE(6,FFF)
     *        (IT(1,J,I),J=1,NC),ITN(1,I),(IT(2,J,I),J=1,NC),ITN(2,I)
200   CONTINUE
C
2000  FORMAT('BROJ LONGITUDINALNIH MODOVA       (NMOD) :',T56,I5)
2001  FORMAT('DUZINA BRZE FFT                   (NFFT) :',T56,I5)
2002  FORMAT('M * delta_t = T                      (M) :',T56,I5)
2003  FORMAT('POJACANJE POJACIVACA                 (A) :',T51,E15.8)
2004  FORMAT('FAKTOR KOSINUS-KVADRATNE FUNKCIJE (BETA) :',T51,E15.8)
2005  FORMAT('VARIJANSA MODULISUCEG IMPULSA   (SIGMA0) :',T51,E15.8)
2006  FORMAT('BROJ IMPULSA JE 2*ISTART+1      (ISTART) :',T56,I5)
2007  FORMAT('RO = b_min / b_max                  (RO) :',T51,E15.8)
2008  FORMAT('UKUPNA IZLAZNA SNAGA LD           (PTOT) :',T51,E15.8)
2009  FORMAT('BINARNI PROTOK                      (FB) :',T51,E15.8)
2010  FORMAT('PROPUSNI OPSEG VLAKNA L=1 km        (B1) :',T51,E15.8)
2011  FORMAT('DISPERZIJA MATERIJALA               (DM) :',T51,E15.8)
2012  FORMAT('SIRINA SPEKTRA LD               (SIGMAI) :',T51,E15.8)
2013  FORMAT('DUZINA VLAKNA                       (RL) :',T51,E15.8)
2014  FORMAT('RAZMAK DVA SUSEDNA MODA         (DLAMBD) :',T51,E15.8)
2015  FORMAT('CENTRALNA TALASNA DUZINA LD     (CLAMBD) :',T51,E15.8)
2016  FORMAT('EFIKASNOST FOTO-DIODE              (ETA) :',T51,E15.8)
2017  FORMAT('SR. VR. LAVINSKOG POJACANJA APD    (GSR) :',T51,E15.8)
2018  FORMAT('SLABLJENJE DUZ VLAKNA L=1km      (ALFA1) :',T51,E15.8)
2019  FORMAT('PARALELNA OTPORNOST APD             (RB) :',T51,E15.8)
2020  FORMAT('PARALELNA KAPACITIVNOST APD         (CD) :',T51,E15.8)
2021  FORMAT('STRUJA MRAKA                     (DCURR) :',T51,E15.8)
2022  FORMAT('POJACANJE BIP TRANZISTORA         (BBIP) :',T51,E15.8)
2023  FORMAT('PARALELNA OTPORNOST POJACIVACA      (RA) :',T51,E15.8)
2024  FORMAT('PARALELNA KAPACITIVNOST POJACIVACA  (CA) :',T51,E15.8)
2025  FORMAT('KOLEKTORSKA STRUJA BIP TRANZ.    (CCURR) :',T51,E15.8)
2026  FORMAT('FAKTOR SUMA APD                      (X) :',T51,E15.8)
2027  FORMAT('STATISTICKI MODEL SUMA PRERASP.   (ISTM) :',T56,I5)
2028  FORMAT('BRZINA SPONTANE EMISIJE             (CR) :',T51,E15.8)
2029  FORMAT('BRZINA STIMULISANE EMISIJE          (AR) :',T51,E15.8)
2030  FORMAT('PRORACUN OPTIMALNOG GSR          (IGOPT) :',T56,I5)
2031  FORMAT('PRORACUN OPTIMALNE Ic            (ICOPT) :',T56,I5)
2032  FORMAT('UKLJUCIVANJE KONTROLNE STAMPE   (ICNTRL) :',T56,I5)
2033  FORMAT('RAZLIKA Ic OD OPTIMALNOG [%]    (CCPERC) :',T51,E15.8)
2034  FORMAT('RAZLIKA Gsr OD OPTIMALNOG [%]   (GSPERC) :',T51,E15.8)
2035  FORMAT('VARIJANSA Ptot                   (SIGPT) :',T51,E15.8)
2036  FORMAT('SREDNJA VREDNOST Ptot            (SRVPT) :',T51,E15.8)
2037  FORMAT('SLABLJENJE NA SPOJEVIMA VLAKNA   (ALFAS) :',T51,E15.8)
2038  FORMAT('DONJA GRANICA APSCISE GRAFIKA     (XMIN) :',T51,E15.8)
2039  FORMAT('GORNJA GRANICA APSCISE GRAFIKA    (XMAX) :',T51,E15.8)
2040  FORMAT('BROJ TACAKA GRAFIKA Pe(Psr)       (NPOI) :',T56,I5)
2041  FORMAT('ZA mBnB KOD  MC=m                   (MC) :',T56,I5)
2042  FORMAT('ZA mBnB KOD  NC=n                   (NC) :',T56,I5)
2043  FORMAT('BROJ MODOVA mBnB KODA              (NMD) :',T56,I5)
C
      RETURN
      END
