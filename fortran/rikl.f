      FUNCTION RIKL(HOUT,HTF,KK,LL,I)
C=============================================================
C FUNKCIJA  : RACUNA UTICAJ I-TOG IMPULSA NA VELICINU       
C             SUMA PRERASPODELE U TRENUTKU t=0
C
C ULAZNE PROMENLJIVE:
C
C   HOUT   = SPEKTAR SIGNALA NA ULAZU U ODLUCIVAC
C   HTF    = TRANSFER FUNKCIJA KOJA POTICE OD PRERASPODELE
C            SNAGE IZMEDJU MODOVA LD
C   KK,LL  = INTEGRAL SE RACUNA ZA K-ti I L-ti MOD
C   I      = RACUNA SE UTICAJ I-TOG IMPULSA U TRENUTKU t=0
C             
C OPIS PROMENLJIVIH:
C
C   NF     = 2**NF=NFFT
C   NFFT   = DUZINA BRZE FURIJEOVE TRANSFORMACIJE
C   DF     = KORAK IZMEDJU ODMERAKA SPEKTRA
C   M      = M*delta_t=T
C   F      = NORMALIZOVANA FREKVENCIJA /0-1/
C   TAU    = TAU = DM * RL * DLAMBD
C   FB     = BINARNI PROTOK
C
C POZIVI PODPROGRAMA:
C
C   FOUREA = KOMPLEKSNA BRZA FURIJEOVA TRANSFORMACIJA
C            -1 --> DIREKTNA ; +1 --> INVERZNA
C   CCONV  = KOMPLEKSNA KONVOLUCIJA ( ZA SPEKTRALNI DOMEN)
C
C POZIVI FUNKCIJA:
C
C   HOUT   = SPEKTAR SIGNALA NA ULAZU U ODLUCIVAC
C   HTF    = TRANSFER FUNKCIJA KOJA POTICE OD PRERASPODELE
C            SNAGE IZMEDJU MODOVA LD
C============================================================
C
      INCLUDE 'FIBER.COM'
      EXTERNAL HOUT,HTF
C
      DOUBLE COMPLEX HOUT,HTF,SPEC1(1024),SPEC2(1024),TEMP
      INTEGER KK,LL,I
      DATA PI/3.1415926/
C
C INICIJALIZACIJA PARAMETARA
C
      FCT=SIGMAL*FB
      DF=1./DBLE(FLOAT(M))
      XI=DBLE(FLOAT(I))
      ARG1=2.*PI*(DBLE(FLOAT(KK))*TAU*FB-XI)
      ARG2=2.*PI*(DBLE(FLOAT(LL))*TAU*FB-XI)
C
C RACUNANJE PODINTEGRALNIH FUNKCIJA
C
      JJ=0
      DO 10 IJ=1,NFFT
         F=DBLE(FLOAT(JJ))*DF
C
         ARG=ARG1*F
         RR=DCOS(ARG)
         RI=DSIN(ARG)
         TEMP=DCMPLX(RR,RI)
         SPEC1(IJ)=HOUT(F)*TEMP/HTF(F)*FCT
C
         ARG=ARG2*F
         RR=DCOS(ARG)
         RI=DSIN(ARG)
         TEMP=DCMPLX(RR,RI)
         SPEC2(IJ)=HOUT(F)*TEMP/HTF(F)*FCT
         JJ=JJ+1
10    CONTINUE
C
C RACUNANJE KOMPLEKSNE KONVOLUCIJE U SPEKTRALNOM DOMENU
C
      CALL CCONV(NFFT,SPEC1,SPEC2)
C
C NALAZENJE INVERZNE FURIJEOVE TRANSFORMACIJE 
C
      CALL FOUREA(SPEC1,NFFT,1)
C
C IMPULSNI ODZIV U TRENUTKU t=0
C
      IF(ICNTRL.NE.0)PRINT *,' ** RIKL: I,K,L=',I,KK,LL,ZABS(SPEC1(1))
C
      RIKL=ZABS(SPEC1(1))
C
      RETURN
      END
